using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerHandler : MonoBehaviour
{
    // Start is called before the first frame update
    public float hpMax = 40;
    public float hp;    

    [Header ("Unity Stuff")]
    public Image playerHealtBar;
    public Canvas HUD;
    public Canvas Pause;
    public Canvas Retry;
    [SerializeField]
    private TextMeshProUGUI text;

    void Start()
    {
        hp = hpMax;
        HUD.enabled = true;
        Retry.enabled = false;
        Pause.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public float GetMaxHealt(){
        return hpMax;
    }

    public float getHealth(){
        return hp;
    }

    public void receiveDamage(int damage){
        hp -= damage;
    }

    public void receiveHeal(int heal){
        hp += heal;
    }

    public void resetStats(){
        hp = hpMax;
        updateHealthBar();
        HUD.enabled = true;
        Retry.enabled = false;
        Pause.enabled = false;
        Time.timeScale = 1;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void OnTriggerEnter(Collider col){
        if(col.gameObject.tag == "EnemySword" && col.gameObject.GetComponent<EnemySword>().getAttack()){      
            receiveDamage (2);
            updateHealthBar();
            if (getHealth() <= 0) {
                Animator playerAnim = GetComponent<Animator> ();
                playerAnim.SetFloat ("Speed", 0);
                playerAnim.SetBool ("Attack1", false);
                playerAnim.SetBool ("Grounded", true);
                playerAnim.SetBool ("Die", true);
                playerAnim.Play ("death");
                StartCoroutine (DelayedRestart (playerAnim.GetCurrentAnimatorStateInfo (0).length));
            }
        }
    }

    public void updateHealthBar(){        
        playerHealtBar.fillAmount = getHealth () / GetMaxHealt ();
    }

    IEnumerator DelayedRestart (float _delay) {
        yield return new WaitForSeconds (_delay);
        Time.timeScale = 0;
        HUD.enabled = false;
        Pause.enabled = false;
        Retry.enabled = true;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void handleGameEnding(){                 
        Time.timeScale = 0;
        HUD.enabled = false;
        Pause.enabled = true;
        Retry.enabled = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        text.text = "-";     
    }
}
