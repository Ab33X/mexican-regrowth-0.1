using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSword : MonoBehaviour {

    private Animator animator;
    private GameObject player;

    void Start () {
        player = GameObject.FindWithTag ("Player");
        animator = player.GetComponent<Animator>();
    }

    public bool getAttack(){
        return animator.GetBool("Attack1");
    }

}