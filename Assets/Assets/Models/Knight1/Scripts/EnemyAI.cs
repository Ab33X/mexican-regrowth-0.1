using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyAI : MonoBehaviour {

    public Transform target;
    public float moveSpeed = 7f;
    Rigidbody rig;
    Animator anim;
    public float hpMax = 100;
    private float hp;
    private Vector3 startingPosition;
    private Vector3 roamingPosition;
    private float positionReachedDistance = 1f;
    private float followingDistance = 30f;
    private float timer;
    private bool chasing = false;
    float x, y, z;
    public GameObject skullPrefab;

    [Header ("Unity Stuff")]
    public Image enemyHealtBar;

    // Start is called before the first frame update
    void Start () {
        rig = GetComponent<Rigidbody> ();
        anim = GetComponent<Animator> ();
        startingPosition = transform.position;
        roamingPosition = GetRoamingPosition ();
        hp = hpMax;
    }

    void Update () {
        timer += Time.deltaTime;
        if (Vector3.Distance (transform.position, target.position) < followingDistance) {
            //Chasing player
            chasing = true;
            Vector3 pos = Vector3.MoveTowards (transform.position, target.position, moveSpeed * Time.fixedDeltaTime);
            if (Vector3.Distance (transform.position, target.position) < positionReachedDistance) {
                //Player position reached
                anim.SetBool ("Moving", false);
                anim.SetBool ("Attack", true);
            } else {
                anim.SetBool ("Moving", true);
                anim.SetBool ("Attack", false);
                rig.MovePosition (pos);
                transform.LookAt (pos);
            }
        } else {
            if (chasing) {
                startingPosition = transform.position;
                chasing = false;
            }
            //Roaming
            Vector3 pos = Vector3.MoveTowards (transform.position, roamingPosition, moveSpeed * Time.fixedDeltaTime);
            if (Vector3.Distance (transform.position, roamingPosition) < positionReachedDistance) {
                //Roam position reached
                anim.SetBool ("Moving", false);
                if (timer >= 5f) {
                    timer = 0;
                    roamingPosition = GetRoamingPosition ();
                }
            } else {
                anim.SetBool ("Moving", true);
                rig.MovePosition (pos);
                transform.LookAt (pos);
            }
        }
    }

    private Vector3 GetRoamingPosition () {
        return startingPosition + GetRandomDirection () * Random.Range (1f, 3f);
    }

    private Vector3 GetRandomDirection () {
        return new Vector3 (UnityEngine.Random.Range (-1f, 1f), 0f, UnityEngine.Random.Range (-1f, 1f)).normalized;
    }

    public float getHealth () {
        return hp;
    }

    public void receiveDamage (int damage) {
        hp -= damage;
    }

    public float GetMaxHealt () {
        return hpMax;
    }

    void OnTriggerEnter (Collider col) {
        if (col.gameObject.tag == "PlayerSword" && col.gameObject.GetComponent<PlayerSword> ().getAttack ()) {
            receiveDamage (10);
            enemyHealtBar.fillAmount = getHealth () / GetMaxHealt ();
            if (getHealth () <= 0) {
                anim.SetBool ("Moving", false);
                anim.SetBool ("Attack", false);
                anim.SetBool ("Die", true);
                anim.Play ("death");
                StartCoroutine (DelayedDead (anim.GetCurrentAnimatorStateInfo (0).length));
            }
        }
    }

    IEnumerator DelayedDead (float _delay) {
        x = transform.position.x;
        y = transform.position.y;
        z = transform.position.z;
        yield return new WaitForSeconds (_delay);
        Vector3 pos = new Vector3 (x, y, z);
        Instantiate (skullPrefab, pos, skullPrefab.transform.rotation);
        Destroy (transform.gameObject);
    }
}