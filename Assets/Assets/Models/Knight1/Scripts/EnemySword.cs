using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySword : MonoBehaviour
{
    private Animator animator;

    void Start()
    {
        animator = transform.parent.gameObject.GetComponent<Animator>();
    }

    public bool getAttack(){
        return animator.GetBool("Attack");
    }
}
