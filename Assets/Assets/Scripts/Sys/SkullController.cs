using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkullController : MonoBehaviour
{
    void OnTriggerEnter(Collider col){
        if( col.gameObject.tag == "Player"){
            col.gameObject.GetComponent<PlayerHandler>().receiveHeal(10);
            col.gameObject.GetComponent<PlayerHandler>().updateHealthBar();
            Destroy(gameObject);
        }
    }
}
