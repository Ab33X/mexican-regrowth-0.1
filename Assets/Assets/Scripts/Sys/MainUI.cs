using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainUI : MonoBehaviour
{
    public void PlayGame () {
        SceneManager.LoadScene(SceneManager.GetActiveScene ().buildIndex + 1);
    }

    public void QuitGame () {
        Application.Quit ();
    }

    public void TryAgain () {
        Main.main.TryAgain();
        SceneManager.LoadScene ("Level1");
    }

    public void MainMenu () {
        SceneManager.LoadScene ("MainMenu");
    }

}
