using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour
{
    public static Main main;
    public GameObject player;
    int score = 0;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if(main == null){
            main = this;
        }else{
            Destroy(gameObject);
            return;
        }
    }    

    void Start()
    {        
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TryAgain(){
        player.GetComponent<PlayerHandler>().resetStats();
    }
}
